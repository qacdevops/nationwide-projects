# Week 3 Challenge

## Tasks

Using Vehicle as a base class, create three derived classes (Car, Motorbike, etc.). Each derived class should have its own attributes in addition to the normal Vehicle attributes.

Using an implementation of your choice, store all your Vehicles in a Garage class.

Create a method in Garage that iterates through each Vehicle, calculating a bill for each type of Vehicle differently, depending on the type of Vehicle it is (this does not need to be complex).

The garage should have methods that add a Vehicle, remove a Vehicle by its ID or its type, fix a Vehicle (which calculates the bill) and to empty the Garage.

## Stretch Goals

Implement code in the Garage class to have a method that removes multiple Vehicles by their type.

## Submission

You should create a public Github repository that contains the code for the above task.

You are also expected to make a README.md file and to fill this README with information of this challenge. 

It should contain the following headers:

- What was the challenge?
- How I expected the challenge to go.
- What went well?
- What didn't go as planned?
- Possible improvements for future challenges