# Week 4 Challenge

## Tasks

For this week's challenge, you are tasked with meeting the following requirements:

- Implement all of the functionality you see on the following image:

![mvp-calculator](images/mvp-calculator.png)

## Stretch Goals

Implement the functionality of the following buttons:

MC (Memory Clear)
MR (Memory Recall)
M+ (Memory Add)
M- (Memory Subtract)

The final result should look something like this:

![stretch-goal-calculator](images/stretch-goal-calculator.png)


## Submission

You should create a public Github repository that contains the code for the calculator.

You are also expected to make a README.md file and to fill this README with information of this challenge. It should contain the following headers.

It should contain the following headers:

- What was the challenge?
- How I expected the challenge to go.
- What went well?
- What didn't go as planned?
- Possible improvements for future challenges