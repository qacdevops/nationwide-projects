# Week 1 Challenge

## Task
For this week's challenge, you should write bash scripts that perform this task.

- Creates a new group on the Linux system
- Creates a new user
- Adds the new user to the group
- Creates a file called run.sh
- Enters an echo command to that file.
- Makes that file executable only by members of your new group
- Switch to you new user
- Runs your run.sh script.


## Stretch Goals
1. Create a Git repository and clone it to your machine
2. Write another script that takes a commit message as user input and pushes any existing changes in the repository to Github.

## Submission
You should create a public Github repository that contains your scripts.
You are also expected to make a README.md file and to fill this README with information of this challenge.

It should contain the following headers:

- What was the challenge?
- How I expected the challenge to go.
- What went well?
- What didn't go as planned?
- Possible improvements for future challenges