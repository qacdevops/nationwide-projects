# Week 3 Challenge

## Task

Your task is to create a Jenkins pipeline that takes this [app](https://gitlab.com/qacdevops/nbs-challenge-3) and on every push to the repository, tests the app, creates the docker image and pushes the image to a Nexus repository.

## Stretch Goals

1. Try to give different versions to each image pushed to the Nexus repository.
2. Create a second pipeline that is triggered every 15 mins, checks if the running image is up to date and if not, deploys the latest image in a container.

## Submission

To submit this challenge you should create a Git repository containing AT LEAST any Jenkinsfiles and scripts you create.

You are also expected to make a README.md file and to fill this README with information about this challenge. It should contain the following headers.

- What was the challenge?
- How I expected the challenge to go.
- What went well?
- What didn't go as planned?
- Possible improvements for future challenges.