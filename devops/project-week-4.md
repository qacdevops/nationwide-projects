# Week 4 Challenge

## Task

Write a playbook that installs Docker onto the local machine.

## Stretch Goals

1. Create a Docker role.
2. Add a role that will deploy an Nginx container.

## Submission

You should create a Git repository that will contain any playbooks you create.
You are also expected to make a README.md file and to fill this README with information of this challenge. It should contain the following headers.

- What was the challenge?
- How I expected the challenge to go.
- What went well?
- What didn't go as planned?
- Possible improvements for future challenges.