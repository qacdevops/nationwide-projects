# Week 2 Challenge

## Task

[Here is a Flask app](https://gitlab.com/qacdevops/trio-task/-/tree/master). Your task is to run this application in a Docker container.

You may notice that the application uses an SQL database. This needs to be in a separate container. All secrets should be secure and not publicly available.

## Stretch Goals

Create a third container that acts as a reverse proxy to the application. (NGINX)

## Submission

You should create a public Github repository that contains your Dockerfiles and a deploy.sh that contains all the commands you executed to run the application.

You are also expected to make a README.md file and to fill this README with information of this challenge.

It should contain the following headers:

- What was the challenge?
- How I expected the challenge to go.
- What went well?
- What didn't go as planned?
- Possible improvements for future challenges.