# Nationwide Projects

A little repo to contain all of the Nationwide cohort's weekly challenges.

The _DevOps Mentored Project_ can be found [here](https://gitlab.com/qacdevops/nbs-mentored-project/-/tree/master).